const database = require('mysql');

const config = {
    host: "localhost",
    user: "root",
    password: "",
    database: "learnnode"
};

const connect = database.createConnection(config);
connect.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
});