
// run script with npm: add script to scripts in package.json to run script with npm run [namescript] Eg: "t": "node index.ejs" => npm run t <=> node index.ejs

const http = require('http');
// Call a module, package use require => list modules: https://www.w3schools.com/nodejs/ref_modules.asp
const fs = require('fs');
const url = require('url');
const test = require('./test.js'); //my package
//install package use npm install [name-package] <=> add package to dependencies in package.json file
//remove package use npm remove [name-package] <=> delete package at dependencies in package.json file
//update: npm update | -g: global-package-installed | -s => silent: chỉ hiển thị output mong muốn khi chạy script
//search: npm search [search word] => all package have search word in name

const uc = require('upper-case');

/**
 * URL module basic use
 * */
// var adr = 'http://localhost:8080/default.htm?year=2017&month=february';
// var q = url.parse(adr, true);
//
// console.log(q.host); //returns 'localhost:8080'
// console.log(q.pathname); //returns '/default.htm'
// console.log(q.search); //returns '?year=2017&month=february'
// var qdata = q.query; //returns an object: { year: 2017, month: 'february' }
// console.log(qdata.month); //returns 'february'
/*end tutorial*/

const hostname = 'localhost';
const port = 3000;

const server = http.createServer((req, res) => {
    // --- Write text
    // res.statusCode = 200;
    // res.setHeader('Content-Type', 'text/plain'); // text
    // res.setHeader('Content-Type', 'text/html'); // html
    // res.write("hello world");
    // res.write("Current date is: " + test.currentDate());
    // res.write(uc.upperCase("Hello World!"));
    // res.end();

    // --- read file
    // fs.readFile('index.html', function(err, data) { //đọc file html
    //     res.writeHead(200, {'Content-Type': 'text/html'});
    //     res.write(data);
    //     res.end();
    // });

    // --- url module
    // let q = url.parse(req.url, true);
    // let filename = '.';
    // if (q.pathname === '/'){ // default url dẫn vào index
    //     filename += '/index.html';
    // }
    // else{
    //     filename += q.pathname;
    // }
    // fs.readFile(filename, function(err, data) {
    //     if (err) {
    //         res.writeHead(404, {'Content-Type': 'text/html'});
    //         return res.end("404 Not Found");
    //     }
    //     res.writeHead(200, {'Content-Type': 'text/html'});
    //     res.write(data);
    //     return res.end();
    // });
});

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
    // console.log(`The text is ${test.show("hehe")}`);
});