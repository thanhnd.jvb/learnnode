const http = require('http');
const routes = require('./routes/routes.js');

const hostname = 'localhost';
const port = 3000;

// load route to app
http.createServer(routes).listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
    console.log(__dirname);
    console.log(__filename);
});