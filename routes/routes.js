const express = require('express');
const controller = require('../controllers/controller.js');

const app = express();
app.set('view engine', 'ejs');
app.route('/')
    .get(function (req, res) {
        // res.send("ok");
        controller.test(req, res);
    });
app.route('/index')
    .get(function (req, res) {
        controller.index(req, res);
    });
app.route('/write')
    .get(function (req, res) {
        controller.writefile(req, res);
    });
app.route('/connect')
    .get(function (req, res) {
        controller.testconnect(req, res);
    });
module.exports = app;