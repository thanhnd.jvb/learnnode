const fs = require('fs');

exports.test = function(req, res) {
    res.send('Test controllers');
};
exports.index = function (req, res) {
    console.log(exports);
    // res.send('Hello world!');
    fs.readFile('./views/index.html', function(err, data) {
        if (err) {
            res.writeHead(404, {'Content-Type': 'text/html'});
            return res.end("404 Not Found");
        }
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write(data);
        return res.end();
    });
};
exports.writefile = function (req, res) {
    let data = "<h1>new file content</h1>";
    fs.writeFile("testreadandwritefile.html", data, (err) => {
        if (err) console.log(err);
        console.log("Successfully Written to File.");
    });
    fs.readFile('./testreadandwritefile.html', function(err, data) {
        if (err) {
            res.writeHead(404, {'Content-Type': 'text/html'});
            return res.end("404 Not Found");
        }
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write(data);
        return res.end();
    });
};
const db = require('../models/model.js')
exports.testconnect = function (req, res) {
    console.log(db);
};