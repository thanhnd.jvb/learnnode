var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var userconnect = 0;

app.get('/', function(req, res){
    res.sendFile(__dirname + '/chatclient.html');
});

io.on('connection', function(socket){
    userconnect++;
    console.log('User connected: ' + userconnect);
    socket.on('disconnect', function(){
        userconnect--;
        console.log('a user disconnected. Now user connected: ' + userconnect);
    });
    socket.on('message', function(msg){
        console.log('message: ' + msg);
        io.emit('messagetoshow', msg); //send to all client use io.emit - socket.emit only send to client sent mess (socket is this client)
    });
});

http.listen(3300, function(){
    console.log('listening on *:3300');
});