var udp = require('dgram');

// -- udp server
var server = udp.createSocket('udp4');

// emits when any error occurs
server.on('error',function(error){
    console.log('Error: ' + error);
    server.close();
});

// event receive message
server.on('message',function(msg, info){
    console.log('Data received from client: ' + msg.toString());
    console.log('Received %d bytes from %s:%d\n',msg.length, info.address, info.port);

    //sending msg
    server.send(msg, info.port,'localhost',function(error){
        if(error){
            client.close();
        }
        else{
            console.log('Data sent from server !!!');
        }
    });
});

//emits when socket is ready and listening for datagram msgs
server.on('listening',function(){
    var address = server.address();
    var port = address.port;
    var family = address.family;
    var ipaddr = address.address;
    console.log('Server is listening at port: ' + port);
    console.log('Server ip: ' + ipaddr);
    console.log('Server is IP4/IP6: ' + family);
});

//emits after the socket is closed using socket.close();
server.on('close',function(){
    console.log('Socket server is closed !');
});

server.bind(2222);

setTimeout(function(){
    console.log('Before close');
    server.close();
},3000);



// udp client
var bf = require('buffer');

// creating a client socket
var client = udp.createSocket('udp4');

client.on('message',function(msg, info){
    console.log('Data received from server: ' + msg.toString());
    console.log('Received %d bytes from %s:%d\n',msg.length, info.address, info.port);
});

setTimeout(function () {
    //buffer msg
    var data = Buffer.from('data mau send');
    var data1 = Buffer.from('hello ');
    var data2 = Buffer.from('world');

    //sending msg
    client.send(data,2222,'localhost',function(error){
        if(error){
            client.close();
        }else{
            console.log('Data sent from client !!!');
        }
    });

    //sending multiple msg
    client.send([data1, data2],2222,'localhost',function(error){
        if(error){
            client.close();
        }else{
            console.log('Multi data sent from client !!!');
        }
    });
}, 2000);