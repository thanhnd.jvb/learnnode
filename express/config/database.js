var mysql = require('mysql');
var config = {
    host: "localhost",
    user: "root",
    password: "",
    database: "learnnode"
};
var connect = mysql.createConnection(config);
connect.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
});
module.exports = connect;