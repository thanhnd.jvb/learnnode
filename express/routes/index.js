var express = require('express');
var loginController = require('../controllers/loginController.js');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('login', {
        title: 'Login'
    });
});

router.post('/', function (req, res, next) {
    loginController.checkLogin(req, res);
});

router.get('/testconnect', function (req, res, next) {
    loginController.test(req, res);
});

module.exports = router;
